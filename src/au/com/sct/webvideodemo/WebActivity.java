package au.com.sct.webvideodemo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebView;

public class WebActivity extends Activity {

    private static final String TAG = WebActivity.class.getSimpleName();

    private static final boolean ENABLE_JAVASCRIPT = true;

    private static final boolean ENABLE_DOM_STORAGE = true;
    private static final boolean ENABLE_APPCACHE = true;
    private static final boolean ENABLE_FORMSAVE = false;
    private static final boolean ENABLE_PASSWORDSAVE = false;

    private static final String DEMO_URL = "http://test1.manichord.com/aopen.html";

    private WebView webcontent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Starting WebActivity");

        // to disable the title bar of the application...
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        // to disable the notification bar and lockscreen...
        getWindow().addFlags(
                LayoutParams.FLAG_FULLSCREEN
                        | LayoutParams.FLAG_DISMISS_KEYGUARD);

        setContentView(R.layout.main);

        setupWebview();
        Log.d(TAG, "Loading DEMO URL " + DEMO_URL);
        webcontent.loadUrl(DEMO_URL);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Log.d(TAG, "Handling orientation change");
        if (webcontent != null) {
            Log.d(TAG, "orientation change - webview reload url "
                    + webcontent.getUrl());
            webcontent.reload();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void setupWebview() {
        webcontent = (WebView) findViewById(R.id.webContent);

        webcontent.getSettings().setJavaScriptEnabled(ENABLE_JAVASCRIPT);
        webcontent.getSettings().setDomStorageEnabled(ENABLE_DOM_STORAGE);
        webcontent.getSettings().setAppCacheEnabled(ENABLE_APPCACHE);
        webcontent.getSettings().setSaveFormData(ENABLE_FORMSAVE);
        webcontent.getSettings().setSavePassword(ENABLE_PASSWORDSAVE);
    }
}